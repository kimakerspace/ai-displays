
FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/ai-jam.git

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]