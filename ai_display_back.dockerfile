
# DOCKER_BUILDKIT=1 docker build -t aib -f ai_display_back.dockerfile .
# docker run -p 5000:80 aib

FROM python:2 as runtime-image

RUN apt-get update && apt-get install -y git
RUN apt-get install build-essential libasound2-dev libjack-dev -y
RUN apt-get install python-dev -y
RUN pip install virtualenv

RUN git clone https://github.com/IMAGINARY/ai-jam.git /ai-jam

WORKDIR /ai-jam
RUN virtualenv --python=/usr/bin/python2.7 env
RUN env/bin/pip install tensorflow==1.12.0 magenta==0.1.15

RUN bash INSTALL.sh

ENTRYPOINT ["bash", "RUN.sh"]