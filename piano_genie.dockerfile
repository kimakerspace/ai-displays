# Build and run:
# DOCKER_BUILDKIT=1 docker build -t piano_genie -f piano_genie.dockerfile .
# docker run -p 5000:80 piano_genie

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/piano-genie.git /app/public

COPY piano_genie.json /app/public/app.json
COPY piano.png /app/public/icons/icon.png

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]