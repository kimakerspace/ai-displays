# ai-displays

Wir haben einige so Monitore da, auf denen wir eine interaktive KI-Austellung machen. Die ganzen Programme dafür kommen von I am AI. Wir haben diese nur hier mal gerade in ein paar Docker Container geworfen, damit das Setup einfacher ist.

## Getting started
Um die Programme zu starten, einfach per docker-compose starten:
```bash
docker-compose up --build
```

Und dann http://localhost:7450/ im Browser öffnen. Wenn das ganze tatsächlich als Ausstellung verwendet werden soll, dann lohnt sich der Kiosk Modus von Firefox:
```bash
chromium --kiosk http://localhost:7450
```

