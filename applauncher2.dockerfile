# Build and run:
# DOCKER_BUILDKIT=1 docker build -t applauncher2 -f applauncher2.dockerfile .
# docker run -p 5000:80 applauncher2

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/applauncher2.git /app/public
COPY config.yaml /app/public/cfg/config.yml

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]