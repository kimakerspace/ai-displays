# Build and run:
# DOCKER_BUILDKIT=1 docker build -t simple-networks -f simple-networks.dockerfile .
# docker run -p 5000:80 simple-networks

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/simple-networks.git /app/public

COPY simple_networks.json /app/public/app.json
COPY simple_network.png /app/public/icons/icon.png

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]