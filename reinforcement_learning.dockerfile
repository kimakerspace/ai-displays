# Build and run:
# DOCKER_BUILDKIT=1 docker build -t reinforcement-learning -f reinforcement-learning.dockerfile .
# docker run -p 5000:80 reinforcement-learning

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/reinforcement-learning-2.git /app/public

# RUN cp -r /app/public/dist/* /app/public

COPY reinforcement_learning.json /app/public/app.json
COPY reinforcement_learning.png /app/public/icons/icon.png

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]