# Build and run:
# DOCKER_BUILDKIT=1 docker build -t neural_numbers -f neural_numbers.dockerfile .
# docker run -p 5000:80 neural_numbers

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/neural-numbers.git /app/public

COPY neural_numbers.json /app/public/app.json
COPY mnist.png /app/public/icons/icon.png

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]