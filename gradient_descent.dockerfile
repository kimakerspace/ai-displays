# Build and run:
# DOCKER_BUILDKIT=1 docker build -t gradient_descent -f gradient_descent.dockerfile .
# docker run -p 5000:80 gradient_descent

FROM nginx:1.21 as runtime-image

RUN apt-get update && apt-get install -y git

RUN git clone https://github.com/IMAGINARY/gradient-descent.git /app/public

COPY gradient_descent.json /app/public/app.json
COPY gradient_descent.png /app/public/icons/icon.png

RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d

ENTRYPOINT ["nginx", "-g", "daemon off;"]